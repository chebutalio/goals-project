import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  @Select(state => state.clicks.clicks) clicks$: Observable<number>;
  @Select(state => state.clicks.inputData.data) inputData$: Observable<string>;

  constructor() { }

  public clicks: number;
  public inputData: string;

  ngOnInit(): void {
    this.clicks$.subscribe((clicks) => {
      this.clicks = clicks;
    });

    this.inputData$.subscribe((data) => {
      console.log(data);
      this.inputData = data;
    });
  }

}
