import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngxs/store';
import {AddClick, AddInputData} from '../store/goals.actions';

@Component({
  selector: 'app-input',
  templateUrl: './app-input.component.html',
  styleUrls: ['./app-input.component.scss']
})
export class AppInputComponent implements OnInit {

  public inputData = new FormControl('');

  constructor(private store: Store) { }

  ngOnInit(): void {
  }

  onInputChanges(inputData): void {
    this.store.dispatch(new AddInputData(inputData.value));
  }

}
