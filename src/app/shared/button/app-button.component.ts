import { Component, OnInit } from '@angular/core';
import {Store} from '@ngxs/store';
import {AddClick} from '../store/goals.actions';

@Component({
  selector: 'app-button',
  templateUrl: './app-button.component.html',
  styleUrls: ['./app-button.component.scss']
})
export class ButtonComponent implements OnInit {
  public counter: number = 0;

  constructor(private store: Store) { }

  ngOnInit(): void {
  }

  onClick(): void {
    this.counter = ++this.counter;

    this.store.dispatch(new AddClick(this.counter));
  }
}
