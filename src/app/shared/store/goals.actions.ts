export class AddClick {
  static readonly type = '[Clicks] AddClick';

  constructor(public payload: number) { }
}

export class AddInputData {
  static readonly type = '[InputData] AddInputData';

  constructor(public payload: string) { }
}
