import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { AddClick, AddInputData } from './goals.actions';

export interface GoalsStateModel {
  clicks: number;
  inputData: {
    data: string,
  };
}

@State<GoalsStateModel>({
  name: 'clicks',
  defaults: {
    clicks: null,
    inputData: {
      data: null,
    }
  },
})

@Injectable()
export class GoalsState {

  @Action(AddClick)
  addClick({patchState}: StateContext<GoalsStateModel>, {payload}: AddClick): void {
    patchState({
      clicks: payload,
    });
  }

  @Action(AddInputData)
  addInputData({patchState}: StateContext<GoalsStateModel>, {payload}: AddInputData): void {
    patchState({
      inputData: {
        data: payload,
      },
    });
  }
}
